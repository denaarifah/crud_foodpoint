<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodPlace extends Model
{
    //Digunakan untuk menggunakan soft delete secara default saat menghapus data
    use SoftDeletes;
    protected $fillable = 
    [
    	'nama','alamat','latitude','longitude','id_menu'
    ];
    protected $dates = ['deleted_at'];
}
