@extends('layouts.app')

@section('content')
<h4>Ubah Konten</h4>
<form action="{{ route('foodplace.update', $foodplace->id) }}" method="post">
    {{csrf_field()}}
    {{ method_field('PUT') }}
    <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
        <label for="nama" class="control-label">Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="nama" value="{{ $foodplace->nama }}">
        @if ($errors->has('nama'))
            <span class="help-block">{{ $errors->first('nama') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
        <label for="alamat" class="control-label">ALamat</label>
        <textarea name="alamat" cols="30" rows="5" class="form-control">{{ $foodplace->alamat }}</textarea>
        @if ($errors->has('alamat'))
            <span class="help-block">{{ $errors->first('alamat') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('latitude') ? 'has-error' : '' }}">
        <label for="latitude" class="control-label">Latitude</label>
        <input type="text" class="form-control" name="latitude" placeholder="latitude" value="{{ $foodplace->latitude }}">
        @if ($errors->has('latitude'))
            <span class="help-block">{{ $errors->first('latitude') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('longitude') ? 'has-error' : '' }}">
        <label for="longitude" class="control-label">Longitude</label>
        <input type="text" class="form-control" name="longitude" placeholder="longitude" value="{{ $foodplace->longitude }}">
        @if ($errors->has('longitude'))
            <span class="help-block">{{ $errors->first('longitude') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('id_menu') ? 'has-error' : '' }}">
        <label for="id_menu" class="control-label">Id Menu</label>
        <input type="text" class="form-control" name="id_menu" placeholder="id_menu" value="{{ $foodplace->id_menu }}">
        @if ($errors->has('id_menu'))
            <span class="help-block">{{ $errors->first('id_menu') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('foodplace.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection